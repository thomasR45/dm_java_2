import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        Integer res = new Integer(a+b);
        return res;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer mini = null;
        for(Integer elem:liste){
            if(mini==null || elem.compareTo(mini)<0){
                mini = elem;
            }
        }
        return mini;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        if(liste.size()==0){
            return true;
        }
        return valeur.compareTo(Collections.min(liste))<0;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> res = new ArrayList<>();

        if(liste1.size()>0 && liste2.size()>0){
            int i1=0;
            int i2=0;
            while(i1<liste1.size() && i2<liste2.size()){
                T val1 = liste1.get(i1);
                T val2 = liste2.get(i2);
                if(val1.compareTo(val2)==0 && !res.contains(val1)){
                    res.add(val1);
                    i1+=1;
                    i2+=1;
                }
                else if(val1.compareTo(val2)<0){
                    i1+=1;
                }
                else{
                    i2+=1;
                }
            }
        }
        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> res = new ArrayList<>();
        String[] l = texte.split(" ");
        for(int i=0;i<l.length;++i){
            if(!l[i].equals("")){
                res.add(l[i]);
            }
        }
        return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){

        List<String> liste = BibDM.decoupe(texte);
        List<String> plusPresents = new ArrayList<>();
        Map<String,Integer> nbOccurences = new HashMap<>();
        String plusPres = null;

        for(String elem:liste){
            if(nbOccurences.containsKey(elem)){
                Integer occ = nbOccurences.get(elem);
                nbOccurences.put(elem,new Integer(occ+1));
            }
            else{
                nbOccurences.put(elem, new Integer(1));
            }
        }
        for(String key:nbOccurences.keySet()){
            if(plusPres==null){
                plusPres = key;
                plusPresents.add(key);
            }
            else if(nbOccurences.get(key).compareTo(nbOccurences.get(plusPres))>0){
                plusPresents.add(key);
                plusPresents.remove(plusPres);
                plusPres = key;
            }
            else if(nbOccurences.get(key).compareTo(nbOccurences.get(plusPres))==0){
                plusPresents.add(key);
            }
        }

        if(plusPresents.size()>1){
            plusPres = Collections.min(plusPresents);
        }
        return plusPres;

        
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int nbPGauche = 0;
        int nbPDroite = 0;
        int ind = 0;
        boolean res = true;
        if(chaine.length()==0){
            res = true;
        }
        else if(chaine.length()>1){
            while(ind<chaine.length() && res){
                if(chaine.charAt(ind)=='('){
                    nbPGauche += 1;
                    if(ind==chaine.length()-1){
                        res = false;
                    }
                }
                else{
                    nbPDroite += 1;
                    if(ind==0){
                        res=false;
                    }
                }
                ind+=1;
            }
            if(nbPGauche != nbPDroite){
                res=false;
            }
        }
        else{
            res = false;
        }

        return res;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        int nbPGauche = 0;
        int nbPDroite = 0;
        int nbCGauche = 0;
        int nbCDroite = 0;
        boolean res = true;
        int ind = 0;

        if(chaine.length()==0){
            res = true;
        }
        else if(chaine.length()==1){
            res = false;
        }
        else{
            while(ind<chaine.length() && res){
                if(chaine.charAt(ind)=='('){
                    nbPGauche += 1;
                    if(ind==chaine.length()-1 || (ind<chaine.length()-1 && chaine.charAt(ind+1)==']')){
                        res = false;
                    }
                }
                else if(chaine.charAt(ind)==')'){
                    nbPDroite += 1;
                    if(ind==0 || (ind>chaine.length()-1 && chaine.charAt(ind-1)=='[')){
                        res=false;
                    }
                }
                else if(chaine.charAt(ind)=='['){
                    nbCGauche += 1;
                    if(ind==chaine.length()-1 || (ind<chaine.length()-1 && chaine.charAt(ind+1)==')')){
                        res=false;
                    }
                }
                else{
                    nbCDroite += 1;
                    if(ind==0 || (ind>chaine.length()-1 && chaine.charAt(ind-1)=='(')){
                        res=false;
                    }
                }

                ind+=1;
            }
            if(nbPGauche != nbPDroite || nbCGauche != nbCDroite){
                res=false;
            }
        }
        return res;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if(liste.size()>0){
            int mini = 0;
            int maxi=liste.size();
            int mid;
            while(mini<maxi){
                mid = ((mini+maxi)/2);
                if(valeur.compareTo(liste.get(mid))==0){
                    return true;
                }
                else if(valeur.compareTo(liste.get(mid))>0){
                    mini = mid+1;
                }
                else{
                    maxi = mid;
                }
            }
        }
        return false;
        
    }



}
